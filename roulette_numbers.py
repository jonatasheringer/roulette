from random import randint
class roulette_class (object):
    def __init__ (self):
                self.debug="off"
                self.color="null"
                self.number=""
                self.numbers_history=[]
                self.numbers_dict = {'1':'red','2':'black',
                                     '3':'red','4':'black',
                                     '5':'red','6':'black',
                                     '7':'red','8':'black',
                                     '9':'red','10':'black',
                                     '12':'red','11':'black','13':'black',
                                     '14':'red','15':'black',
                                     '16':'red','17':'black',
                                     '18':'red','20':'black',
                                     '19':'red','22':'black',
                                     '21':'red','24':'black',
                                     '23':'red','26':'black',
                                     '25':'red','28':'black',
                                     '27':'red','29':'black',
                                     '30':'red','31':'black',
                                     '32':'red','33':'black',
                                     '34':'red','35':'black',
                                     '36':'red','0':'green','37':'green'}
                self.bets_dict={'1-18':[1,2,3],
                                '19-36':'1',
                                '1-12':'',
                                '13-24':'',
                                '25-36':'',
                                'even':'',
                                'odd':'',
                                '1-34':'',
                                '2-35':'',
                                '3-36':'',
                            }
    def get_spin(self):
        #spins roulette
        number=randint(0, 37)
        self.color=self.numbers_dict[str(number)] #sets color of number
        if (self.debug=="on"):print "get_spin(",number,self.color,")"
        self.numbers_history.append(number)
        self.number=number
        return number
        
    def get_color(self):
        #returns the color of last number
        return self.color
    def get_history(self):
        if (self.debug=="on"):print self.numbers_history
        return self.numbers_history
    